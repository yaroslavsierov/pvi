function changeForm(title){
    let formTitle = document.getElementById("title");
    let formButton = document.getElementById("form-button");
    if (title === "Edit") {
    
      formButton.textContent = "Save";
      formTitle = "Edit student";
    } else if (title === "Add") {
      formTitle = "Add student";
      formButton.textContent = "Create";
      formButton.onclick = addRow;
    }
  
  }
  
 var lastId;

  function animateNotification() {
    const element = document.getElementById("notifications")
  
    element.style.transitionDuration = "0.5s"
    element.style.content = " ";
    element.style.position = "absolute";
    element.style.top = "14px";
    element.style.right = "17px";
    element.style.width = "10px";
    element.style.height = "10px";
    element.style.borderRadius = "50%";
    element.style.background = "orangered";
  }



  function addRow() {

  
    let id = window.lastId;
    let group = document.getElementById("group").value;
    let firstName = document.getElementById("first_name").value;
    let lastName = document.getElementById("last_name").value;
    let gender = document.getElementById("gender").value;
    let birthday = document.getElementById("birthday").value;
    let student = new Student(id, group, firstName, lastName, gender, birthday);
  
   var ID = student.id;
   var Group = student.group;
   var First = student.firstName;
   var Last = student.lastName;
   var Gender = student.gender;
   var Birthday = student.birthday;

   

    //`````````
    const formData = {
      Id: ID,
      Firstname: First,
      Lastname: Last,
      Group_name: Group,
      Gender: Gender,
      Birthday: Birthday
    };
 

     
    let responce;

      responce =  serverCreateStudent(student);
      console.log(responce);

    closeForm();
    
}

  
  function showToast(message, bg_color = "#ffffff") {
    var toast = document.getElementById("toast");
    var header = document.getElementById("toast-header");
    var body = document.getElementById("toast-body");
  
    // Налаштування стилів
    header.style.backgroundColor = bg_color;
    body.innerText = message;
  
    // Приховати та показати віконце спливаючого повідомлення
    toast.style.visibility = "visible";
    toast.style.opacity = "1";
    setTimeout(function() {
      toast.style.visibility = "hidden";
      toast.style.opacity = "0";
    }, 5000);
  }

  function hideToast() {
    var toast = document.getElementById("toast");
    toast.style.visibility = "hidden";
    toast.style.opacity = 0;
  }

  

  class Student {
    constructor(id, group, firstName, lastName, gender, birthday) {
      this.id = id;
      this.group = group;
      this.firstName = firstName;
      this.lastName = lastName;
      this.gender = gender;
      this.birthday = birthday;
    }
    
    getName() {
      return this.firstName + ' ' + this.lastName;
    }



  toObject(){
      return {
        Id: this.id,
        Group_name: this.group,
        Firstname: this.firstName,
        Lastname: this.lastName,
        Gender: this.gender,
        Birthday: this.birthday,
      }
  }
  }
  
    function fromJson(data){
      return new Student(data.Id, data.Group_name, data.Firstname, data.Lastname, data.Gender, data.Birthday)
  }

  
  let StudentList = [];
  
  function showAddForm() {
    document.getElementById("first_name").value = null;
    document.getElementById("last_name").value = null;
    document.getElementById("birthday").value = null;
    document.getElementById("group").value = "";
  document.getElementById("gender").value = "";
    document.getElementById("myForm").style.display = "block";
    document.getElementById("overlay").style.display = "block";
  
  }
  
  function showEditForm(button) {
  var row = button.closest("tr").rowIndex - 1;
  
  document.getElementById("first_name").value = StudentList[row].firstName;
  document.getElementById("last_name").value = StudentList[row].lastName;
  document.getElementById("group").value = StudentList[row].group;
  document.getElementById("gender").value = StudentList[row].gender;
  document.getElementById("birthday").value = StudentList[row].birthday;
  
  let formButton = document.getElementById("form-button");
  formButton.onclick = function () { editStudent(row); };
  
  document.getElementById("myForm").style.display = "block";
  document.getElementById("overlay").style.display = "block";
  
  }

  function showWarning(button) {
    document.getElementById("deleteForm").style.display = "block";
    document.getElementById("overlay").style.display = "block";
  
    const formButton = document.getElementById("delete-button");
    formButton.onclick = function () {
      deleteRow(button);
    };
  }

  function editStudent(row) {

    let group = document.getElementById("group").value;
    let firstName = document.getElementById("first_name").value;
    let lastName = document.getElementById("last_name").value;
    let gender = document.getElementById("gender").value;
    let birthday = document.getElementById("birthday").value;
  
    StudentList[row].group = group;
    StudentList[row].firstName = firstName;
    StudentList[row].lastName = lastName;
    StudentList[row].gender = gender;
    StudentList[row].birthday = birthday

    var ID = StudentList[row].id;
    var Group = StudentList[row].group;
    var First = StudentList[row].firstName;
    var Last = StudentList[row].lastName;
    var Gender = StudentList[row].gender;
    var Birthday = StudentList[row].birthday;
 
    
 
     //`````````
     const formData = {
      Id: ID,
      Firstname: First,
      Lastname: Last,
      Group_name: Group,
      Gender: Gender,
      Birthday: Birthday
    };
  
      
      
      let responce;
      responce = serverEditStudent(StudentList[row], row);
      console.log(responce);
     /* if(responce.ok){
          showToast(`Successfully  edited a student`, "#a4d0a4");
          var cells = document.getElementById("myTable").rows[row+1].cells;
          cells[1].innerHTML = StudentList[row].group;
          cells[2].innerHTML = StudentList[row].getName();
          cells[3].innerHTML = StudentList[row].gender;
          cells[4].innerHTML = StudentList[row].birthday;
          checkAllBox();  
      }
      else
      {
        showToast("Incorrect data", "#d56f7a");
      }*/
      closeForm();
  }
  
  function setMaxDateForBirthdayInput() {
    var today = new Date();
    var maxDate = new Date(today.getFullYear() - 17, today.getMonth(), today.getDate()).toISOString().split('T')[0];
    var minDate = new Date(1950, 0, 2).toISOString().split('T')[0];
    document.getElementById("birthday").setAttribute("max", maxDate);
    document.getElementById("birthday").setAttribute("min", minDate);
  }
  
  function deleteRow(button) {
    var table = document.getElementById("myTable");
    var row = button.parentNode.parentNode.rowIndex-1;
    
    console.log("Student deleted: " + StudentList[row].id + ". " + StudentList[row].group + ", " + StudentList[row].gender + ", " + StudentList[row].birthday);

    //```````````````
    serverDeleteStudent(StudentList[row].id);
    StudentList.splice(row, 1);
    table.deleteRow(row+1);
    //```````````````````````````
    checkAllBox();
    closeWarning();
  }
  
  
  
  function closeForm() {
    document.getElementById("myForm").style.display = "none";
    document.getElementById("overlay").style.display = "none";
  }
  
function checkAllBox() {
    var table = document.getElementById("myTable");
    var checkboxes = table.getElementsByTagName("input");
    var allChecked = true;
    for (var i = 1; i < checkboxes.length; i++) {
      if (checkboxes[i].type === "checkbox" && !checkboxes[i].checked) {
        allChecked = false;
        break;
      }
    }
    document.getElementById("check-all").checked = allChecked;
  }
  
  function toggleCheckboxes(checkbox) {
    var table = document.getElementById("myTable");
    var checkboxes = table.getElementsByTagName("input");
    for (var i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].type === "checkbox" && checkboxes[i].parentNode.cellIndex === checkbox.parentNode.cellIndex) {
        checkboxes[i].checked = checkbox.checked;
      }
    }
  }
  
  
  function closeWarning() {
    document.getElementById("deleteForm").style.display = "none";
    document.getElementById("overlay").style.display = "none";
  }
  
 // window.addEventListener('load', getres);

/*function getres() {
  fetch('http://localhost/student/getall', {
    method: 'GET',
  })
  .then(response => response.json())
  .then(data => {
    console.log(data);
    
    var table = document.getElementById("myTable");
    for(let n = 0; n < data.length; n++)
    {
      var row = table.rows.length;
      var checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.onclick = function () { checkAllBox(); };

      let student = new Student(data[n].Id, data[n].Group, data[n].Firstname, data[n].Lastname, data[n].Gender, data[n].Birthday);

      StudentList.push(student);
      var cell1 = table.insertRow(-1).insertCell(0);
      var cell2 = table.rows[row].insertCell(1);
      var cell3 = table.rows[row].insertCell(2);
      var cell4 = table.rows[row].insertCell(3);
      var cell5 = table.rows[row].insertCell(4);
      var cell6 = table.rows[row].insertCell(5);
      var cell7 = table.rows[row].insertCell(6);
      cell1.appendChild(checkbox);
      cell2.innerHTML = data[n].Group;
      cell3.innerHTML = data[n].Firstname + " " + data[n].Lastname;
      cell4.innerHTML = data[n].Gender;
      cell5.innerHTML = data[n].Birthday;
      cell6.innerHTML = "<span class='indicator'></span>";
      cell7.innerHTML = "<button class='option' onclick=\"changeForm('Edit');showEditForm(this)\"><i class='fa fa-pencil'></i></button><button class='option' onclick='showWarning(this)'><i class='fa fa-times'></i></button>";
      checkAllBox();
    }
    window.lastId = data[data.length-1].Id+1;
  })
  .catch(error => {
    showToast("Undefined error", "#FF5733");
    console.error('Error:', error);
  })}

function ajax(url, method, data1, callback, student) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4) {
      if (this.status == 200) {
        console.log("Успішно: ", this.responseText);
        callback(this.responseText);
        StudentList.push(student);
         var table = document.getElementById("myTable");
         var checkbox = document.createElement("input");
         checkbox.type = "checkbox";
         checkbox.onclick = function () { checkAllBox(); };
         var row = table.rows.length;
         window.lastId++;
         var cell1 = table.insertRow(-1).insertCell(0);
         var cell2 = table.rows[row].insertCell(1);
         var cell3 = table.rows[row].insertCell(2);
         var cell4 = table.rows[row].insertCell(3);
         var cell5 = table.rows[row].insertCell(4);
         var cell6 = table.rows[row].insertCell(5);
         var cell7 = table.rows[row].insertCell(6);
         cell1.appendChild(checkbox);
         cell2.innerHTML = student.group;
         cell3.innerHTML = student.getName();
         cell4.innerHTML = student.gender;
         cell5.innerHTML = student.birthday;
         cell6.innerHTML = "<span class='indicator'></span>";
         cell7.innerHTML = "<button class='option' onclick=\"changeForm('Edit');showEditForm(this)\"><i class='fa fa-pencil'></i></button><button class='option' onclick='showWarning(this)'><i class='fa fa-times'></i></button>";
         checkAllBox();
      } else {
        console.error("Помилка: ", this.status, this.statusText);
        showToast("Incorrect data", "#FF5733");
      }
    }
  };
  xhttp.open(method, url, true);
  xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send(JSON.stringify(data1));
}

  function deleteStudent(studentId) {
    fetch(`http://localhost:5000/student/${studentId}`, {
      method: 'DELETE',
    })
    
    .then(data => {
      console.log(data);
      return true;
      // обробляємо відповідь сервера
    })
    .catch(error => {
      showToast("Undefined error", "#FF5733");
      console.error('Error:', error);
      return false;
    });
  }*/
  


  function createStudent(student, status = "offline"){  
    StudentList.push(student);
    var table = document.getElementById("myTable");
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.onclick = function () { checkAllBox(); };
    var row = table.rows.length;
    //window.lastId++;
    var cell1 = table.insertRow(-1).insertCell(0);
    var cell2 = table.rows[row].insertCell(1);
    var cell3 = table.rows[row].insertCell(2);
    var cell4 = table.rows[row].insertCell(3);
    var cell5 = table.rows[row].insertCell(4);
    var cell6 = table.rows[row].insertCell(5);
    var cell7 = table.rows[row].insertCell(6);
    cell1.appendChild(checkbox);
    cell2.innerHTML = student.group;
    cell3.innerHTML = student.getName();
    cell4.innerHTML = student.gender;
    cell5.innerHTML = student.birthday;
    cell6.innerHTML = "<span class='indicator'></span>";
    cell7.innerHTML = "<button class='option' onclick=\"changeForm('Edit');showEditForm(this)\"><i class='fa fa-pencil'></i></button><button class='option' onclick='showWarning(this)'><i class='fa fa-times'></i></button>";
    checkAllBox();
  }

  async function serverGetStudents(){
    let responce = await fetch("server.php")
    let responce_data = await responce.json()

    if (responce.ok) { return responce_data}
    else { showToast(responce_data["data"], "#d56f7a"); return null}
}

window.onload = async () => {
  console.log("server is loading")
  setMaxDateForBirthdayInput();
  let students = await serverGetStudents();
  console.log("server has loaded")
  if(students){
      students.forEach(student => {createStudent(fromJson(student)); console.log(student)});
  }
  window.lastId = parseInt(students[students.length-1].Id) + 1;
}



async function serverEditStudent(student, row){
  let responce = await fetch("server.php", {
      headers: {"Accept": "application/json", "Content-Type": "application/json",},
      method: "PUT",
      body: JSON.stringify(student.toObject())
  })

  let responce_data = await responce.json()

  if (responce.ok) 
  {
    var cells = document.getElementById("myTable").rows[row+1].cells;
    cells[1].innerHTML = StudentList[row].group;
    cells[2].innerHTML = StudentList[row].getName();
    cells[3].innerHTML = StudentList[row].gender;
    cells[4].innerHTML = StudentList[row].birthday;
    checkAllBox();  
     return responce_data["data"];
    
    }
  else { showToast(responce_data["message"], "#d56f7a"); return null}
}

async function serverDeleteStudent(id){
  let responce = await fetch("server.php", {
      headers: {"Accept": "application/json", "Content-Type": "application/json",},
      method: "DELETE",
      body: JSON.stringify({"Id": id}) 
  })

  let responce_data = await responce.json()

  if (responce.ok) { return responce_data}
  else { showToast(responce_data["data"], "#d56f7a"); return null}
}
async function serverCreateStudent(student){
  let responce = await fetch("server.php", {
      headers: {"Accept": "application/json", "Content-Type": "application/json",},
      method: "POST",
      body: JSON.stringify(student.toObject())
  })

  let responce_data = await responce.json()

  if (responce.ok) 
  {
    StudentList.push(student);
    var table = document.getElementById("myTable");
    var checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.onclick = function () { checkAllBox(); };
    var row = table.rows.length;
    window.lastId++;
    var cell1 = table.insertRow(-1).insertCell(0);
    var cell2 = table.rows[row].insertCell(1);
    var cell3 = table.rows[row].insertCell(2);
    var cell4 = table.rows[row].insertCell(3);
    var cell5 = table.rows[row].insertCell(4);
    var cell6 = table.rows[row].insertCell(5);
    var cell7 = table.rows[row].insertCell(6);
    cell1.appendChild(checkbox);
    cell2.innerHTML = student.group;
    cell3.innerHTML = student.getName();
    cell4.innerHTML = student.gender;
    cell5.innerHTML = student.birthday;
    cell6.innerHTML = "<span class='indicator'></span>";
    cell7.innerHTML = "<button class='option' onclick=\"changeForm('Edit');showEditForm(this)\"><i class='fa fa-pencil'></i></button><button class='option' onclick='showWarning(this)'><i class='fa fa-times'></i></button>";
    checkAllBox();


     return responce_data["data"];
    }
  else { showToast(responce_data["message"], "#d56f7a"); return null}
}