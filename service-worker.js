const CACHE = "my-site-cache-v1";
const FILES_TO_CACHE = ["/", "/Students.html", "/Style.css", "/script.js"];

self.addEventListener('install', async (event) => {
  const cache = await caches.open(CACHE)
  await cache.addAll(FILES_TO_CACHE)
});

self.addEventListener('activate', async (event) => {
    const cacheNames = await caches.keys()
    await Promise.all(
      cacheNames
        .filter(name => name !== CACHE)
        .map(name => caches.delete(name))
    )
});

async function cacheFirst(request) {
  const cached = await caches.match(request)
  return cached ?? await fetch(request)
}

self.addEventListener('fetch', async (event) => {
  event.respondWith(cacheFirst(event.request))
});
