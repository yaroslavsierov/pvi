<?php
$http_responce_code_map = ["success" => 200, "error" => 400];
$err = array("Status" => "error", "data" => "Nothing :)");

try {
    $conn = new mysqli("localhost", "root", "1111", "sys");
    if (!$conn) {
        $err["data"] = "CONNECTION ERROR!!!!";
        echo json_encode(array("status" => "error", "data" => "The connection error"));
    }

    function validate_student($data): array
    {
        $keys = array("Firstname", "Lastname", "Group_name", "Gender", "Birthday");

        foreach ($keys as $key) {
            if (empty($data->$key)) {
                return array("status" => "error", "message" => "Field " . $key . " not set");
            }
        }

        $name_pattern = "/^[A-Za-z]+$/";

        if (
            !preg_match($name_pattern, $data->Firstname) ||
            !preg_match($name_pattern, $data->Lastname)
        ) {
            return array("status" => "error", "message" => "Name fields do not match the pattern");
        } else {
            return array("status" => "success", "data" => $data);
        }
    }

    if($_SERVER["REQUEST_METHOD"] == "GET"){
        $result = $conn->query("select * from users");
        echo json_encode($result->fetch_all(mode: MYSQLI_ASSOC));
    } else if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $body = file_get_contents('php://input');
        $object = json_decode($body);
        $validated = validate_student($object);
        http_response_code($http_responce_code_map[$validated["status"]]);
    
        if ($validated["status"] == "success") {
            $query = $conn->prepare(
                "INSERT INTO users(Id, Firstname, Lastname, Group_name, Gender, Birthday) values (?, ?, ?, ?, ?, ?)"
            );
            $query->bind_param(
                "isssss",
                $object->Id,
                $object->Firstname,
                $object->Lastname,
                $object->Group_name,
                $object->Gender,
                $object->Birthday
            );
            $query->execute();
        }
        
        header('Content-Type: application/json'); 
        echo json_encode($validated);
    }   else if ($_SERVER["REQUEST_METHOD"] == "PUT") {
        $body = file_get_contents('php://input');
        $object = json_decode($body);
        $validated = validate_student($object);
        http_response_code($http_responce_code_map[$validated["status"]]);
        if ($validated["status"] == "success") {
            $query = $conn->prepare(
                "UPDATE users SET Firstname = ?, Lastname = ?, Group_name = ?, Gender = ?, Birthday = ? WHERE Id = ?"
            );
            $query->bind_param(
                "sssssi",
                $object->Firstname,
                $object->Lastname,
                $object->Group_name,
                $object->Gender,
                $object->Birthday,
                $object->Id
            );
            $query->execute();
        }
        header('Content-Type: application/json');
        echo json_encode($validated);
    }
    else if($_SERVER["REQUEST_METHOD"] == "DELETE"){
        $body = file_get_contents("php://input");
        $object = json_decode($body);

        if(!isset($object->Id)){
            $responce = array("status" => "error", "message" => "Field Id is not set");
            
        } else{
            $query = $conn->prepare("delete from users where Id = ?");
            $query->bind_param("i", $object->Id);
            $query->execute();
            $responce = array("status" => "success");
        }

        http_response_code($http_responce_code_map[$responce["status"]]);
        echo json_encode($responce);
    }
} catch (Exception $e) {
    http_response_code(401);
    echo json_encode(array("status" => "error", "data" => $e->getMessage()));
}
?> 
