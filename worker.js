export default function registerServiceWorker() {
    window.addEventListener("load", async () => {
      if ("serviceWorker" in navigator) {
        try {
          await navigator.serviceWorker.register("/service-worker.js");
          console.log("[SW] registered");
        } catch (error) {
          console.error(`Registration failed with ${error}`);
        }
      }
    });
  }
  